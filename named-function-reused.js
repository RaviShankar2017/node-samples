console.log('Before');
getUser(1, _getRepositories);
console.log('After');

function _getRepositories(user) {
  console.log("Users:", user);
  getRepositories(user.gitHubUsername, _getCommits);
}

function _getCommits(repos) {
    console.log("repos:", repos);
    getCommits(repos[0], displayCommits);
}

function displayCommits(commits) {
  console.log("commits:", commits);
}

function getUser(id, callback) {
  setTimeout(()=> {
    console.log('Reading a user from a database...');
    callback({ id: id, gitHubUsername: 'Ravi Shankar' });
  }, 2000);
}

function getRepositories(username, callback) {
  setTimeout(()=> {
    console.log('getRepositories: Calling GitHub API...');
    callback(['repo1', 'repo2', 'repo3']);
  }, 2000);
}

function getCommits(repo, callback) {
  setTimeout(()=> {
    console.log('getCommits: Calling GitHub API...');
    callback(['commit1', 'commit2', 'commit3']);
  }, 2000);
}