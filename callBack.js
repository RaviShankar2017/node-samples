console.log('Before');

// Callback-based approach
getUser(1, function(user) {
  getRepositories(user.gitHubUsername, function(repos) {
    getCommits(repos[0], function(commits) {
      console.log(commits);
    })
  })
});

function getUser(id,callback) {
    // Kick off some async work
    setTimeout(function() {
        console.log('Reading a user from a database...');
        callback({ id: id, gitHubUsername: 'mosh' });
    }, 2000);
}

function getRepositories(username, callback) {
    setTimeout(function() {
        console.log('getRepositories: Calling GitHub API...');
        callback(['repo1', 'repo2', 'repo3']);
    }, 2000);
}

function getCommits(repo,callback) {
    setTimeout(function() {
        console.log('getCommits: Calling GitHub API...');
        callback(['commit']);
    }, 2000);
}