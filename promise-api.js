const p1 = new Promise(function(resolve){
    setTimeout(function(){
        console.log("Async operation1.........");
        resolve(1);
    }, 3000);
});

const p2 = new Promise(function(resolve,reject){
    setTimeout(function(){
        console.log("Async operation2.........");
        // resolve({id: 2});
        reject(new Error("Error happend...."));
        // reject(new Error("Error because something is failed"));
    }, 2000);
});

// Either all resolve or at least one rejected
Promise.all([p1, p2])
    .then(function(result) {
        console.log("Result:", result);
    })
    .catch(function(err) {
        console.log("Error:", err.message);
    });

// All of the given promises have either resolved or rejected
// Promise.allSettled([p1, p2])
//     .then(function(result) {
//         console.log("Result:", result);
//     })
//     .catch(function(err) {
//         console.log("Error:", err.message);
//     });

// Called as soon as one of them promise is either resolved or rejected.
// Promise.race([p1, p2])
//     .then(function(result) {
//         console.log("Result:", result);
//     })
//     .catch(function(err) {
//         console.log("Error:", err.message);
//     });


// At least one promise get fulfilled or resolved doesn't matters if others are rejected.
// Promise.any([p1, p2])
//     .then(function(result) {
//         console.log("Result:", result);
//     })
//     .catch(function(err) {
//         console.log("Error:", err.message);
//     });