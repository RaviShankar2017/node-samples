console.log('Before');
// Promise-based approach
getUser(1)
    .then(user => {getRepositories(user.gitHubUsername);})
    .then(repos => {getCommits(repos[0]);})
    .then(commits => {console.log(commits);})
    .catch(err => {console.log("Error:", err);});

// getUser(1)
//     .then(function(user) {
//         getRepositories(user.gitHubUsername)
//     })
//     .then(function(repos) {
//         console.log("repos", repos);
//         getCommits(repos[0])
//     })
//     .then(function(commits) {
//         console.log('Commits', commits)
//     })
//     .catch(function(err) {
//         console.log('Error', err.message)
//     });

console.log('After');

function getUser(id) {
    return new Promise(function(resolve, reject) {
        // Kick off some async work
        setTimeout(function() {
            console.log('Reading a user from a database...');
            resolve({ id: id, gitHubUsername: 'Ravi Shankar' });
        }, 2000);
    });
}

function getRepositories(username) {
    return new Promise((resolve, reject) => {
        console.log('getRepositories: Calling GitHub API...');
        setTimeout(function() {
            console.log("-------------------------------------------------------->>>>");
            resolve(['repo1', 'repo2', 'repo3']);
        }, 2000);
    });
}

function getCommits(repo) {
    return new Promise((resolve, reject) => {
        console.log('getCommits: Calling GitHub API...');
        setTimeout(function() {
            console.log("-------------------->>>>");
            resolve(['commit 1', 'commit 2']);
        }, 2000);
    });
}