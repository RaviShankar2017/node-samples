const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongo-exercises', { useNewUrlParser: true, useUnifiedTopology: true });

const courseSchema = new mongoose.Schema({
  name: String,
  author: String, 
  tags: [ String ],
  date: Date, 
  isPublished: Boolean,
  price: Number
});

const Course = mongoose.model('Course', courseSchema);

function generateObjectId() {
    // _id is a 12 bytes hexadecimal number which assures the uniqueness of every document.
//     You can provide _id while inserting the document.
//     If you don’t provide then MongoDB provides a unique id for every document.
//     These 12 bytes:
//     - first 4 bytes for the current timestamp,
//     - next 3 bytes for machine id,
//     - next 2 bytes for process id of MongoDB server
//     - remaining 3 bytes are simple incremental VALUE.
    const objectId = new mongoose.Types.ObjectId;
    console.log("objectId:", objectId);
    console.log("objectId:Timestamp:", objectId.getTimestamp());
}

async function getCourses() {
    return await Course
        .find({ isPublished: true })
        .or([ { tags: 'frontend' }, { tags: 'backend' } ])
        .sort('-price')
        .select('name author price');
}

async function run() {
    generateObjectId();
    const courses = await getCourses();
    console.log(JSON.stringify(courses));
}

run();
