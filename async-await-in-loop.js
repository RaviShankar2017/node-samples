// This sample code demonstrate to process the async await in loop

var fs = require('fs');

// All type of for loop in JavaScript
// (var i=0; i<array.length;i++) {}
// for (const item of array) {}
// array.forEach(async (item) => {});

const readFileAsArray = function(file, cb = () => {}) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, function(err, data) {
            if (err) {
                reject(err);
                return cb(err);
            }
            const lines = data.toString().trim().split('\n');
            resolve(lines);
            cb(null, lines);
        });
    });
};

// using for loop
async function countOddNumbersUsingForLoop() {
    try {
        var oddCount = 0;
        const files = ['numbers.txt', 'numbers1.txt'];
        for(const value of files) {
            console.log("File Name:", value);
            let lines = await readFileAsArray('./sample-data/' + value);
            let numbers = lines.map(Number);

            oddCount = oddCount + numbers.filter(n => n%2 === 1).length;
        }
        console.log('Odd numbers count:', oddCount);
    } catch(err) {
        console.error(err);
    }
}

// forEach doesn't support with async await.
// JavaScript proceeds to call console.log('Odd numbers count:', oddCount); before the
// promises in the forEach loop gets resolved.
// i.e because forEach is not promise-aware. It cannot support async and await.
// You cannot use await in forEach.
async function countOddNumbersUsingForEach() {
    try {
        var oddCount = 0;
        const files = ['numbers.txt', 'numbers1.txt'];
        files.forEach(async value => {
            console.log("File Name:", value);
            let lines = await readFileAsArray('./sample-data/' + value);
            let numbers = lines.map(Number);

            oddCount = oddCount + numbers.filter(n => n%2 === 1).length;
        });
        console.log('Odd numbers count:', oddCount);
    } catch(err) {
        console.error(err);
    }
}


// Using Map
async function countOddNumbersUsingMap() {
    try {
        var oddCount = 0;
        const files = ['numbers.txt', 'numbers1.txt'];

        const promises = files.map(async value => {
            console.log("File Name:", value);
            let lines = await readFileAsArray('./sample-data/' + value);
            let numbers = lines.map(Number);

            return oddCount = oddCount + numbers.filter(n => n%2 === 1).length;
        });

        await Promise.all(promises);

        console.log('Odd numbers count:', oddCount);
    } catch(err) {
        console.error(err);
    }
}

// countOddNumbersUsingForLoop();

// countOddNumbersUsingForEach();

// countOddNumbersUsingMap();

const EventEmitter = require('events');
const eventEmitter = new EventEmitter();

eventEmitter.once('start', () => {
    console.log('started')
});

eventEmitter.emit('start');
eventEmitter.emit('start');
