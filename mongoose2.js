/*
1:- Schema
2:- Create document
3:- Select document
4:- Count document
5:- Pagination
 */

const mangoose = require('mongoose');

mangoose.connect('mongodb://localhost/mongo-exercises', { useNewUrlParser: true, useUnifiedTopology: true });

const courseSchema = new mangoose.Schema({
    name: { type: String },
    author: {type: String },
    tags: [ String ],
    date: { type: Date, default: Date.now },
    isPublished: { type: Boolean}
});

const Course = mangoose.model('Course', courseSchema);

// Inserting to mongoDB document
async function createCourse() {
    const course = new Course({
        name: "Node.js",
        author: "New author",
        tags: ['Mongo', 'Backend'],
        isPublished: true
    });

    const result = await course.save();
    console.log("createCourse:===>", result)
}

// Getting record from mongodb document.
async function getCourse() {
    const courses = await Course
        .find({author: 'Mosh', isPublished: true})
        .limit(10)
        .sort({ name: 1 })
        .select({name: 1, tags: 1});
    console.log("getCourse: ==>", courses);

    // eq (equal)
    // ne (not equal)
    // gt (greater than)
    // gte (greater than or equal to)
    // lt (less than)
    // lte (less than or equal to)
    // in
    // nin (not in)


    const courses1 = await Course
        // .find({ price: {$gte: 10, $lte: 20}}) // price between 10 and 20
        .find({ price: {$in: [10, 15, 20]}})
        .limit(10)
        .sort({ name: 1 })
        .select({name: 1, tags: 1});
    console.log("courses1====>>>",courses1);

    // or
    // and

    // for AND we can use two way:
    // 1: and([{ author: 'Mosh'}, {isPublished: true}]) // author='Mosh' and isPublished=true
    // 2: find({ author: 'Mosh'}, {isPublished: true})
    const courses2 = await Course
        .find()
        .or([{ author: 'Mosh'}, {isPublished: true}]) // author='Mosh' or isPublished=true
        // .and([{ author: 'Mosh'}, {isPublished: true}]) // author='Mosh' and isPublished=true
        .limit(10)
        .sort({ name: 1 })
        .select({name: 1, tags: 1});
    console.log("courses2====>",courses2);


    // Regular expression
    // starts with Ravi: author: /^Ravi/
    // ends with Ravi(i indicates case insencitive): author: /Ravi$/i
    // Contains Ravi: /.*Ravi.*/i
    const courses3 = await Course
        .find({ author: /^Ravi/})
        .limit(10)
        .sort({ name: 1 })
        .select({name: 1, tags: 1});
    console.log("courses3:==>",courses3);
}

async function getCourseCount() {
    const coursesCount = await Course
        .find({author: 'Mosh', isPublished: true})
        .limit(10)
        .count();
    console.log("courses count:==>", coursesCount);
}

async function getPagination() {
    const pageNumber = 2,
        pageSize = 10;

    const coursesPagination = await Course
        .find({author: 'Mosh', isPublished: true})
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)
        .sort({name: 1})

    console.log("getPagination:==>", coursesPagination);
}

async function run() {
    createCourse();
    getCourse();
    getCourseCount();
    getPagination();
}

run();
