console.log("Starts program");  // Main Task

setTimeout(function() { console.log("setTimeout 1");},0);  // Task Queue

setTimeout(function() { console.log("setTimeout 2");},0);  // Task Queue 

setImmediate(function() { console.log("setImmediate"); });

process.nextTick(function() { console.log("nextTick..1"); });  // Microtask (nextTick will be heigher priority than Promise)

Promise.resolve().then(r => console.log("Promise 1")).then(s => console.log("Promise 2"));  // Microtask

process.nextTick(function() { console.log("nextTick...2"); });  // Microtask

console.log("Ends program")

/*===========================
OUTPUT
----------------------------
Start
End
nextTick..1
nextTick...2
Promise 1
Promise 2
setTimeout 1
setTimeout 2
setImmediate
===============================*/