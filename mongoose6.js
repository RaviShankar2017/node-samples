/*
1:- MongoDB Validation
2:- Custom Validator
3:- Custom Async Validator
4:- schema type options
    String: required, minlength, maxlength, enum, lowercase, uppercase, trim
    Number: min, max
5: getter and setter
 */
//Current Video: 107

const mangoose = require('mongoose');

mangoose.connect('mongodb://localhost/mongo-exercises', { useNewUrlParser: true, useUnifiedTopology: true });

const courseSchema = new mangoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 25
    },
    category: {
        type: String,
        required: true,
        enum: ['web', 'mobile', 'network'],
        lowercase: true,
        // uppercase: true,
        trim: true
    },
    // Custom Async Validator
    author: {
        type: String,
        validate: {
            validator: (value) => {
                return new Promise(function(resolve, reject) {
                    setTimeout(() => {
                        const result = value && value.length > 4;
                        resolve(result);
                    }, 4000)
                });
            },
            message: "Author name should have more than 4 character"
        }
    },

    // Custom Validator
    tags: {
        type: Array,
        validate: {
            validator: function(value) {
                return value && value.length > 0;
            },
            message: 'A course should have at least one tag.'
        }
    },
    date: { type: Date, default: Date.now },
    isPublished: { type: Boolean},
    price: {
        type: Number,
        required: function() { return this.isPublished; },
        min: 10,
        max: 200,
        get: value => Math.round(value),
        set: value => Math.round(value)
    }
});

const Course = mangoose.model('Course', courseSchema);

// Inserting to mongoDB document
async function createCourse() {
    const course = new Course({
        name: "Amazon Web Services",
        category: ' Web',
        author: "RaviS",
        tags: ['Mongo', 'Backend'],
        isPublished: true,
        price: 15.8
    });

    try {
        const result = await course.save();
        console.log("result:", result)
    }
    catch(err) {
        console.log("Error:", err.message);
    }
}

async function run() {
    createCourse();
}

run();
