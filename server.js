/**
 * Module dependencies.
 */

var cluster = require('cluster');

if(cluster.isMaster) {
    var cpuCount = require('os').cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster.fork();
    });
}
else {
    var express = require('express'),
        http = require('http');

    global.app = express();
    app.get('/getUser', function (req, res) {
        res.status(200).send({name: 'Ravi Shankar', contactNumber: '999996765'})
    });

    app.set('port',  3002);

    // ####This makes it easy to provide both HTTP and HTTPS versions of your app with the same code base,
    // ####as the app does not inherit from these (it is simply a callback):
    // var server = http.createServer(app)
    // server.listen(app.get('port'), function() {
    //     console.log('Express server listening on port: ' + app.get("port"));
    // });

    // OR
    app.listen(app.get('port'), function() {
        console.log('Express server listening on port: ' + app.get("port"));
    });
}
