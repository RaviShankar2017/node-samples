const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mongo-exercises', { useNewUrlParser: true, useUnifiedTopology: true });

const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    tags: [ String ],
    date: Date,
    isPublished: Boolean,
    price: Number
});

const Course = mongoose.model('Course', courseSchema);

// Approach: 1
// query First using findById()
// modify its properties
// save()
async function updateCourse_1(id) {
    const course = await Course.findById(id);
    if(!course) return;

    // updating properties by using:
    course.isPublished = true;
    course.author = 'Ravi Shankar1';

    // OR

    // course.set({
    //     isPublished: true,
    //     author: 'RaviShankar1'
    // })

    const result = await course.save();

    return result;
}

// Approach: 2
// update first
// update directly
// optionally: get the updated document
async function updateCourse_2(id) {
    // return await Course.updateOne({ _id: id}, {
    return await Course.updateMany({ _id: id}, {
        $set: {
            author: 'Ravi',
            isPublished: false
        }
    })
}

// Approach: 3
// findByIdAndUpdate
async function updateCourse_3(id) {
    const course = await Course.findByIdAndUpdate(id, {
        $set: {
            author: 'RaviS',
            isPublished: true
        }
    }, { new: true});

    // { new: true} will gives updated document(row)
    return course;
}

async function removeCourse(id) {
    // const result = await Course.deleteOne({ _id: id });
    // const result = await Course.deleteMany({ isPublished: false });
    // return result;

    return await Course.findByIdAndRemove(id); // returns deleted course object
}

async function run() {
    // const result = await updateCourse_1('5e6396cab6a0914fe43de0df');
    // const result = await updateCourse_2('5e6396cab6a0914fe43de0df');
    // const result = await updateCourse_3('5e6396cab6a0914fe43de0df');
    const result = await removeCourse('5e5fe88201c0123280392bc6');
    console.log(result);
}

run();
